import Vue    from 'vue'
import router from './router'

import jQuery from 'jquery'
global.jQuery = jQuery
let Bootstrap = require('bootstrap')
import 'bootstrap/dist/css/bootstrap.css'

//import 'bulma/css/bulma.css'

import App              from './App'
import Navigation       from '@/components/Navigation.vue'

Vue.config.productionTip = false

Vue.component('ticket-main-navigation', Navigation);
/* eslint-disable no-new */
var vm = new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
