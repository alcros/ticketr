import Vue              from 'vue'
import Router           from 'vue-router'
import TicketList       from '@/components/TicketList.vue'
import TicketAdd        from '@/components/TicketAdd.vue'
import TicketForm       from '@/components/TicketForm.vue'
import Reports          from '@/components/Reports.vue'
import Users            from '@/components/Users.vue'
import AreaSettings     from '@/components/AreaSettings.vue'
import PrioritySettings from '@/components/PrioritySettings.vue'
import StatusSettings   from '@/components/StatusSettings.vue'
import CategorySettings from '@/components/CategorySettings.vue'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/ticket-list',     component: TicketList },
    { path: '/ticket-add',      component: TicketAdd },
    { path: '/ticket-edit/:id', component: TicketForm },
    { path: '/reports',   component: Reports },
    { path: '/user',      component: Users },
    { path: '/area',      component: AreaSettings, props: true },
    { path: '/priority',  component: PrioritySettings },
    { path: '/status',    component: StatusSettings,   props: true },
    { path: '/category',  component: CategorySettings, props: true }
//    {
//      path: '/',
//      name: 'Hello',
//      component: Hello
//    }
  ]
})
