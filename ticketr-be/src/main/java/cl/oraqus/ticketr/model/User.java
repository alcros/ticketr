package cl.oraqus.ticketr.model;

import org.springframework.data.annotation.Id;

public class User {
    @Id
    private String id;
    private String name;
    private String lastname;
    private String alias;
    private String email;

    public User() {
    }

    public User(String id, String name, String lastname, String alias, String email) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.alias = alias;
        this.email = email;
    }

    public User(String name, String lastname, String alias, String email) {
        this.name = name;
        this.lastname = lastname;
        this.alias = alias;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAlias() {
        return alias;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", alias='").append(alias).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
