package cl.oraqus.ticketr.model;

import org.springframework.data.annotation.Id;

/**
 * Created by carlos on 02-07-17.
 */
public class Status {
    @Id
    private String id;
    private String name;

    public Status() {}
    public Status(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Status{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
