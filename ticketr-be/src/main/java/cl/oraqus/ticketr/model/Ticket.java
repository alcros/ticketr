package cl.oraqus.ticketr.model;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Ticket {
    @Id
    private String id;
    private String title;
    private String issue;
    private User user;
    private String area;
    private String status;
    private String category;
    private String priority;
    private String comments;
    private Date reportedAt;
    private Date completedAt;
    private boolean resolved = false;

    public Ticket() { }

    public Ticket(String id, User user, String area, String title, String issue,
                  String status, String category, String priority, Date reportedAt) {
        this.id = id;
        this.title = title;
        this.issue = issue;
        this.reportedAt = reportedAt;
        this.user = user;
        this.area = area;
        this.status = status;
        this.category = category;
        this.priority = priority;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Ticket{");
        sb.append("id='").append(id).append('\'');
        sb.append(", user=").append(user);
        sb.append(", title='").append(title).append('\'');
        sb.append(", status=").append(status);
        sb.append(", priority=").append(priority);
        sb.append(", resolved=").append(resolved);
        sb.append(", comments=").append(comments);
        sb.append('}');
        return sb.toString();
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setCompletedAt(Date completedAt) {
        this.completedAt = completedAt;
    }

    public void setResolved(boolean resolved) { this.resolved = resolved; }

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getArea() {
        return area;
    }

    public String getTitle() {
        return title;
    }

    public String getIssue() {
        return issue;
    }

    public String getStatus() {
        return status;
    }

    public String getCategory() {
        return category;
    }

    public String getPriority() {
        return priority;
    }

    public String getComments() {
        return comments;
    }

    public Date getReportedAt() {
        return reportedAt;
    }

    public Date getCompletedAt() {
        return completedAt;
    }

    public boolean isResolved() { return resolved; }
}
