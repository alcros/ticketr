package cl.oraqus.ticketr.model;

import org.springframework.data.annotation.Id;

/**
 * Created by carlos on 02-07-17.
 */
public class Category {
    @Id
    private String id;
    private String name;

    public Category() {}
    public Category(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Category{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
