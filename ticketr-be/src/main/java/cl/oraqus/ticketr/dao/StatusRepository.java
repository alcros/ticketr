package cl.oraqus.ticketr.dao;

import cl.oraqus.ticketr.model.Status;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by carlos on 02-07-17.
 */
public interface StatusRepository extends MongoRepository<Status, String> {

    Status findById(String id);
    Status findByName(String name);
    List<Status> findAll();
    Status insert(Status status);
    void delete(String id);

}