package cl.oraqus.ticketr.dao;

import cl.oraqus.ticketr.model.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by carlos on 02-07-17.
 */
public interface CategoryRepository extends MongoRepository<Category, String> {

    Category findById(String id);
    Category findByName(String name);
    List<Category> findAll();
    Category insert(Category category);
    void delete(String id);

}