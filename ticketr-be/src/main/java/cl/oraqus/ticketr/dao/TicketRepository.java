package cl.oraqus.ticketr.dao;

import cl.oraqus.ticketr.model.Status;
import cl.oraqus.ticketr.model.Ticket;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by carlos on 02-07-17.
 */
public interface TicketRepository extends MongoRepository<Ticket, String> {

    Ticket findById(String id);
    List<Ticket> findAll();
    Ticket insert(Ticket ticket);
    Ticket save(Ticket ticket);
    void delete(String id);

}