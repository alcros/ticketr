package cl.oraqus.ticketr.dao;

import cl.oraqus.ticketr.model.Priority;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by carlos on 02-07-17.
 */
public interface PriorityRepository extends MongoRepository<Priority, String> {

    Priority findById(String id);
    Priority findByName(String name);
    List<Priority> findAll();
    Priority insert(Priority priority);
    void delete(String id);

}