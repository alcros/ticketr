package cl.oraqus.ticketr.dao;

import cl.oraqus.ticketr.model.Area;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by carlos on 02-07-17.
 */
public interface AreaRepository extends MongoRepository<Area, String> {

    Area findById(String id);
    Area findByName(String name);
    List<Area> findAll();
    Area insert(Area area);
    void delete(String id);

}