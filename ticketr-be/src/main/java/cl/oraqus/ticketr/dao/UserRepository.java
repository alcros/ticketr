package cl.oraqus.ticketr.dao;

import cl.oraqus.ticketr.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {
    User findById(String id);
    User findByAlias(String alias);
    List<User> findAll();
    User insert(User user);
    void delete(String id);
}