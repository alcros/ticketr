package cl.oraqus.ticketr.service;

import java.util.List;
import java.util.Optional;

public interface SettingsService<T> {
    List<T> fetchAll();
    Optional<T> fetchById(String id);
    Optional<T> store(T newElement);
    void delete(String id);
}
