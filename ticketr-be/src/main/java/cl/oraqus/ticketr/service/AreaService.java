package cl.oraqus.ticketr.service;

import cl.oraqus.ticketr.dao.AreaRepository;
import cl.oraqus.ticketr.model.Area;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by carlos on 09-07-17.
 */
@Service
public class AreaService implements SettingsService<Area> {

    private static Logger logger = LoggerFactory.getLogger(AreaService.class);

    private AreaRepository repo;

    @Autowired
    public AreaService(AreaRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<Area> fetchAll() {
        logger.debug("fetchAll::");
        return repo.findAll();
    }

    @Override
    public Optional<Area> store(Area newArea) {
        logger.debug("store:: {}", newArea);
        String identifier = (newArea.getId() == null ? UUID.randomUUID().toString() : newArea.getId());
        Area area = new Area(identifier, newArea.getName());
        return Optional.of(repo.insert(area));
    }

    @Override
    public void delete(String id) {
        logger.debug("delete:: id: {}", id);
        repo.delete(id);
    }

    @Override
    public Optional<Area> fetchById(String id) {
        logger.debug("fetchById:: id: {}", id);
        return Optional.of(repo.findById(id));
    }

    public Optional<Area> fetchByName(String name) {
        logger.debug("fetchByName:: name: {}", name);
        Area result = repo.findByName(name);
        return Optional.ofNullable(result);
    }
}