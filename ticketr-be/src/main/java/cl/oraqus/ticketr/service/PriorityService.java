package cl.oraqus.ticketr.service;

import cl.oraqus.ticketr.dao.PriorityRepository;
import cl.oraqus.ticketr.model.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by carlos on 09-07-17.
 */
@Service
public class PriorityService implements SettingsService<Priority> {

    private static Logger logger = LoggerFactory.getLogger(PriorityService.class);

    private PriorityRepository repo;

    @Autowired
    public PriorityService(PriorityRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<Priority> fetchAll() {
        logger.debug("fetchAll::");
        return repo.findAll();
    }

    @Override
    public Optional<Priority> store(Priority newPriority) {
        logger.debug("store:: {}", newPriority);
        String identifier = (newPriority.getId() == null ? UUID.randomUUID().toString() : newPriority.getId());
        Priority area = new Priority(identifier, newPriority.getName());
        return Optional.of(repo.insert(area));
    }

    @Override
    public void delete(String id) {
        logger.debug("delete:: id: {}", id);
        repo.delete(id);
    }

    @Override
    public Optional<Priority> fetchById(String id) {
        logger.debug("fetchById:: id: {}", id);
        return Optional.of(repo.findById(id));
    }

    public Optional<Priority> fetchByName(String name) {
        logger.debug("fetchByName:: name: {}", name);
        Priority result = repo.findByName(name);
        return Optional.ofNullable(result);
    }
}