package cl.oraqus.ticketr.service;

import cl.oraqus.ticketr.dao.CategoryRepository;
import cl.oraqus.ticketr.model.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by carlos on 09-07-17.
 */
@Service
public class CategoryService implements SettingsService<Category> {

    private static Logger logger = LoggerFactory.getLogger(CategoryService.class);

    private CategoryRepository repo;

    @Autowired
    public CategoryService(CategoryRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<Category> fetchAll() {
        logger.debug("fetchAll::");
        return repo.findAll();
    }

    @Override
    public Optional<Category> store(Category newCategory) {
        logger.debug("store:: {}", newCategory);
        String identifier = (newCategory.getId() == null ? UUID.randomUUID().toString() : newCategory.getId());
        Category area = new Category(identifier, newCategory.getName());
        return Optional.of(repo.insert(area));
    }

    @Override
    public void delete(String id) {
        logger.debug("delete:: id: {}", id);
        repo.delete(id);
    }

    @Override
    public Optional<Category> fetchById(String id) {
        logger.debug("fetchById:: id: {}", id);
        return Optional.of(repo.findById(id));
    }

    public Optional<Category> fetchByName(String name) {
        logger.debug("fetchByName:: name: {}", name);
        Category result = repo.findByName(name);
        return Optional.ofNullable(result);
    }
}