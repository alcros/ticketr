package cl.oraqus.ticketr.service;

import cl.oraqus.ticketr.dao.TicketRepository;
import cl.oraqus.ticketr.model.Ticket;
import cl.oraqus.ticketr.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TicketService implements SettingsService<Ticket> {

    private static Logger logger = LoggerFactory.getLogger(TicketService.class);

    private TicketRepository repo;
    private UserService userService;

    @Autowired
    public TicketService(TicketRepository repo) {
        this.repo = repo;
    }

    @Autowired
    public void setUserService(UserService newUserService) {
        this.userService = newUserService;
    }

    @Override
    public List<Ticket> fetchAll() {
        logger.debug("fetchAll::");
        return repo.findAll();
    }

    @Override
    public Optional<Ticket> store(Ticket newTicket) {
        logger.debug("store:: incoming data: {}", newTicket);

        Optional<User> user = userService.fetchByAlias(newTicket.getUser().getAlias());
        if (!user.isPresent()) {
            user = userService.store(newTicket.getUser());
            logger.debug("store:: saved the user included in the ticket as new, user: {}", user);
        }

        Ticket ticket = new Ticket(
                newTicket.getId() == null ? UUID.randomUUID().toString() : newTicket.getId(),
                user.get(),
                newTicket.getArea(), newTicket.getTitle(),
                newTicket.getIssue(),
                newTicket.getStatus() == null ? "Open" : newTicket.getStatus(),
                newTicket.getCategory(),
                newTicket.getPriority(),
                newTicket.getReportedAt() == null ? new Date() : newTicket.getReportedAt()
                );
        ticket.setComments(newTicket.getComments());
        if (newTicket.isResolved()) {
            ticket.setResolved(newTicket.isResolved());
            ticket.setCompletedAt(new Date());
        }
        logger.debug("store:: {}", ticket);
        return Optional.of(repo.save(ticket));
    }

    @Override
    public void delete(String id) {
        logger.debug("delete:: id: {}", id);
        repo.delete(id);
    }

    @Override
    public Optional<Ticket> fetchById(String id) {
        logger.debug("fetchById:: id: {}", id);
        Ticket result = repo.findById(id);
        logger.debug("fetchById:: found: {}", result);
        return Optional.of(result);
    }

    public Optional<Ticket> fetchByName(String name) {
        logger.debug("fetchByName:: name: {}", name);
        throw new UnsupportedOperationException();
    }
}
