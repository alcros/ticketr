package cl.oraqus.ticketr.service;

import cl.oraqus.ticketr.dao.StatusRepository;
import cl.oraqus.ticketr.model.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by carlos on 09-07-17.
 */
@Service
public class StatusService implements SettingsService<Status> {

    private static Logger logger = LoggerFactory.getLogger(StatusService.class);

    private StatusRepository repo;

    @Autowired
    public StatusService(StatusRepository newRepo) {
        this.repo = newRepo;
    }

    @Override
    public List<Status> fetchAll() {
        logger.debug("fetchAll::");
        return repo.findAll();
    }

    @Override
    public Optional<Status> store(Status newStatus) {
        logger.debug("store:: {}", newStatus);
        String identifier = (newStatus.getId() == null ? UUID.randomUUID().toString() : newStatus.getId());
        Status status = new Status(identifier, newStatus.getName());
        return Optional.of(repo.insert(status));
    }

    @Override
    public void delete(String id) {
        logger.debug("delete:: id: {}", id);
        repo.delete(id);
    }

    @Override
    public Optional<Status> fetchById(String id) {
        logger.debug("fetchById:: id: {}", id);
        return Optional.of(repo.findById(id));
    }

    public Optional<Status> fetchByName(String name) {
        logger.debug("fetchByName:: name: {}", name);
        Status result = repo.findByName(name);
        return Optional.ofNullable(result);
    }
}