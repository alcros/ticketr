package cl.oraqus.ticketr.service;

import cl.oraqus.ticketr.dao.UserRepository;
import cl.oraqus.ticketr.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements SettingsService<User> {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserRepository repo;

    @Autowired
    public UserService(UserRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<User> fetchAll() {
        logger.debug("fetchAll::");
        return repo.findAll();
    }

    @Override
    public Optional<User> store(User newUser) {
        logger.debug("store:: {}", newUser);
        String identifier = (newUser.getId() == null ? UUID.randomUUID().toString() : newUser.getId());
        User user = new User(identifier, newUser.getName(), newUser.getLastname(), newUser.getAlias(), newUser.getEmail());
        logger.debug("store:: user service is inserting into the repository, user: {}", user);

        return Optional.of(repo.insert(user));
    }

    @Override
    public void delete(String id) {
        logger.debug("delete:: id: {}", id);
        repo.delete(id);
    }

    @Override
    public Optional<User> fetchById(String id) {
        logger.debug("fetchById:: id: {}", id);
        return Optional.of(repo.findById(id));
    }

    public Optional<User> fetchByAlias(String alias) {
        logger.debug("fetchByAlias:: alias: {}", alias);
        User result = repo.findByAlias(alias);
        return Optional.ofNullable(result);
    }
}