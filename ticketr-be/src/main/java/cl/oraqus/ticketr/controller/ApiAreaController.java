package cl.oraqus.ticketr.controller;

import cl.oraqus.ticketr.model.Area;
import cl.oraqus.ticketr.service.AreaService;
import cl.oraqus.ticketr.service.CategoryService;
import cl.oraqus.ticketr.service.SettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class ApiAreaController {
    private final Logger logger = LoggerFactory.getLogger(ApiAreaController.class);

    private SettingsService service;

    @Autowired
    @Qualifier("areaService")
    public void setService(SettingsService newService) {
        this.service = newService;
    }

    @RequestMapping(path = "/areas", method = RequestMethod.GET)
    public List<Area> retrieve() {
        logger.info("retrieve:: {}", "all");
        return service.fetchAll();
    }

    @RequestMapping(path = "/areas/{id}", method = RequestMethod.GET)
    public Area retrieve(@PathVariable String id) {
        logger.debug("retrieve:: id: {}", id);
        Optional optional = service.fetchById(id);
        logger.debug("retrieve:: {}", optional.get());
        return (Area) optional.get();
    }

    @RequestMapping(path = "/areas", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> store(@RequestBody Area area) {
        logger.debug("store:: area: {}", area);
        //TODO: validate
        AreaService serviceImpl = (AreaService) service;
        Optional<Area> optional = serviceImpl.fetchByName(area.getName());

        if (optional.isPresent()) {
            logger.debug("store:: result: {}", optional);
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

        optional = service.store(new Area(null, area.getName()));
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(optional.get().getId()).toUri();
        logger.debug("store:: location: {}", location);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        ResponseEntity<Area> responseEntity = new ResponseEntity<Area>(optional.get(), headers, HttpStatus.CREATED);
        return responseEntity;
    }

    @DeleteMapping("/areas/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        logger.debug("delete:: id: {}", id);
        service.delete(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="There was an error processing the request body.")
    public void handleMessageNotReadableException(HttpServletRequest request, HttpMessageNotReadableException exception) {
        logger.debug("\nUnable to bind post data sent to: " + request.getRequestURI() + "\nCaught Exception:\n" + exception.getMessage());
    }
}
