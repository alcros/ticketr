package cl.oraqus.ticketr.controller;

import cl.oraqus.ticketr.model.Area;
import cl.oraqus.ticketr.model.Ticket;
import cl.oraqus.ticketr.service.SettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * Created by carlos on 02-07-17.
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class ApiTicketController {
    private final Logger logger = LoggerFactory.getLogger(ApiTicketController.class);

    private SettingsService service;

    @Autowired
    @Qualifier("ticketService")
    public void setSettingsService(SettingsService newService) {
        this.service = newService;
    }

    //curl 'http://localhost:8080/api/tickets' | jq '.[]|.user'
    @RequestMapping(path = "/tickets", method = RequestMethod.GET)
    public List<Ticket> retrieve() {
        logger.debug("retrieve:: all");
        return service.fetchAll();
    }

    @RequestMapping(path = "/tickets/{id}", method = RequestMethod.GET)
    public Ticket retrieve(@PathVariable String id) {
        logger.debug("retrieve:: id: {}", id);
        Optional optional = service.fetchById(id);
        //TODO: implement NotFound when id is null or not found
        logger.debug("retrieve:: {}", optional.get());
        return (Ticket) optional.get();
    }

    //curl -i -H 'Content-Type: application/json' -d '{ "title" : "The big issue", "issue" : "big", "area" : "51", "reportedAt":"2017-08-09'T'20:59:59-0500", "user" : { "name":"Wile", "lastname" : "Coyote", "alias" : "Supergenius", "email" : "coyote@acme.com" } }' -X POST http://localhost:8080/api/tickets
    @RequestMapping(path = "/tickets", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> store(@RequestBody Ticket ticket) {
        logger.debug("store:: incoming ticket: {}", ticket);

        Optional<Ticket> optional = service.store(ticket);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(optional.get().getId()).toUri();
        logger.debug("store:: return location in header: {}", location);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        ResponseEntity<Ticket> responseEntity = new ResponseEntity<Ticket>(optional.get(), headers, HttpStatus.CREATED);
        return responseEntity;
    }

    @RequestMapping(path = "/tickets/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody Ticket ticket) {
        logger.debug("update:: {}, incoming ticket: {}", id, ticket);
        Optional<Ticket> optional = service.store(ticket);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(optional.get().getId()).toUri();
        logger.debug("update:: return location in header: {}", location);

        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<Ticket> responseEntity = new ResponseEntity<Ticket>(optional.get(), headers, HttpStatus.OK);
        return responseEntity;
    }

    @DeleteMapping("/tickets/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        logger.debug("delete:: id: {}", id);
        service.delete(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="There was an error processing the request body.")
    public void handleMessageNotReadableException(HttpServletRequest request, HttpMessageNotReadableException exception) {
        logger.debug("\nUnable to bind post data sent to: " + request.getRequestURI() + "\nCaught Exception:\n" + exception.getMessage());
    }
}
