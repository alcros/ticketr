package cl.oraqus.ticketr.controller;

import cl.oraqus.ticketr.model.Status;
import cl.oraqus.ticketr.service.SettingsService;
import cl.oraqus.ticketr.service.StatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * Created by carlos on 02-07-17.
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class ApiStatusController {
    private final Logger logger = LoggerFactory.getLogger(ApiStatusController.class);

    private SettingsService service;

    @Autowired
    @Qualifier("statusService")
    public void setService(SettingsService newService) {
        this.service = newService;
    }

    @RequestMapping(path = "/statuses", method = RequestMethod.GET)
    public List<Status> retrieve() {
        return service.fetchAll();
    }

    @RequestMapping(path = "/statuses/{id}", method = RequestMethod.GET)
    public Status retrieve(@PathVariable String id) {
        logger.debug("retrieve:: id: {}", id);
        Optional optional = service.fetchById(id);
        logger.debug("retrieve:: {}", optional.get());
        return (Status) optional.get();
    }

    @RequestMapping(path = "/statuses", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> store(@RequestBody Status status) {
        logger.debug("store:: status: {}", status);
        StatusService serviceImpl = (StatusService) service;
        Optional<Status> optional = serviceImpl.fetchByName(status.getName());

        if (optional.isPresent()) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        optional = service.store(new Status(null, status.getName()));
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(optional.get().getId()).toUri();

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        ResponseEntity<Status> responseEntity = new ResponseEntity<Status>(optional.get(), headers, HttpStatus.CREATED);
        return responseEntity;
    }

    @DeleteMapping("/statuses/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        logger.debug("delete:: id: {}", id);
        service.delete(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="There was an error processing the request body.")
    public void handleMessageNotReadableException(HttpServletRequest request, HttpMessageNotReadableException exception) {
        logger.debug("\nUnable to bind post data sent to: " + request.getRequestURI() + "\nCaught Exception:\n" + exception.getMessage());
    }
}
