FROM azul/zulu-openjdk-alpine
#FROM delitescere/java
#FROM delitescere/jdk

LABEL maintainer Oraqus <oraqus.29@gmail.com>

#RUN set -x && apk update && apk add --no-cache bash
#RUN addgroup -S web && adduser -S -G web web
#RUN mkdir /srv

ENV JAVA_OPTS=""

#COPY ticketr-be/target/ticketr-be-0.0.1-SNAPSHOT.jar /srv/app.jar
COPY ticketr-be/target/ticketr-be-1.0.0-SNAPSHOT.jar app.jar

#WORKDIR /srv

#RUN chown web:web -R /srv

#USER web
EXPOSE 8080

#CMD [ "-J-server", "-J-Xms32M", "-J-Xmx64m" ]

ENTRYPOINT [ "sh", "-c", "java -XX:+PrintFlagsFinal -XX:+PrintGCDetails $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar" ]
